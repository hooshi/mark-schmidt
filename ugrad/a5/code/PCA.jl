include("misc.jl")
include("findMin.jl")


#####################################################
############ Orthogonal LeastSquares PCA ############
#= Since Ws are orthogonal, we use SVD instead of
gradient descent. Also since Ws are orthogonal 
(WW') = 1 and inv(WW')=1 so we can just compress 
using x*W'  instead of finding Zs that minimize ZW-X
=#
#####################################################
function PCA(X,k)
    (n,d) = size(X)

    # Subtract mean
    mu = mean(X,1)
    X -= repmat(mu,n,1)

    (U,S,V) = svd(X)
    W = V[:,1:k]'

    compress(Xhat) = compressFunc(Xhat,W,mu)
    expand(Z) = expandFunc(Z,W,mu)

    return CompressModel(compress,expand,W)
end

function compressFunc(Xhat,W,mu)
    (t,d) = size(Xhat)
    Xcentered = Xhat - repmat(mu,t,1)
    return Xcentered*W' # Assumes W has orthogonal rows
end

function expandFunc(Z,W,mu)
    (t,k) = size(Z)
    return Z*W + repmat(mu,t,1)
end






#####################################################
################ LeastSquares PCA ###################
#####################################################
function PCA_gradient(X,k)
    (n,d) = size(X)

    # Subtract mean
    mu = mean(X,1)
    X -= repmat(mu,n,1)

    # Initialize W and Z
    W = randn(k,d)
    Z = randn(n,k)

    R = Z*W - X
    f = sum(R.^2)
    funObjZ(z) = pcaObjZ(z,X,W)
    funObjW(w) = pcaObjW(w,X,Z)
    for iter in 1:50
        fOld = f

        # Update Z
        Z[:] = findMin(funObjZ,Z[:],verbose=false,maxIter=10)

        # Update W
        W[:] = findMin(funObjW,W[:],verbose=false,maxIter=10)

        R = Z*W - X
        f = sum(R.^2)
        @printf("Iteration %d, loss = %f\n",iter,f/length(X))

        if (fOld - f)/length(X) < 1e-2
            break
        end
    end


    # We didn't enforce that W was orthogonal so we need to optimize to find Z
    #HS: i.e. we can't just multiply, we have to solve a linear regression. #Using Gradient descent. When Ws are orthogonal, we can just do Xhat*W'
    #because orthogonal W means WW' = 1 (i.e. inv(WW') = 1 too)
    compress(Xhat) = compress_gradientDescent(Xhat,W,mu)
    
    expand(Z) = expandFunc(Z,W,mu)

    return CompressModel(compress,expand,W)
end

function compress_gradientDescent(Xhat,W,mu)
    (t,d) = size(Xhat)
    Xcentered = Xhat - repmat(mu,t,1)
    # k is the number of parts passed to the function calling this by us
    Z = zeros(t,k)

    funObj(z) = pcaObjZ(z,Xcentered,W)
    Z[:] = findMin(funObj,Z[:],verbose=false)
    return Z
end


#############################################################
#= HS. According to MM.: 
For ALL other differentiable loss functions of the formf(ZW), 
the gradients will have similar forms as LeastSquares 
gradient of loss  except  that R will change
=#
##############################################################


#####################################################
###### LeastSquares gradient of loss w.r.t to Z #####
# Gradient(f,Z) = ZWW' - XW'
# Z = XW' * inv(WW')
# or in julia:
# Z = ( WW' \ WX' )'  Based on my linear algebra skills
# HS. MM. NOTE: For ALL other differentiable loss functions of the form f(ZW), the gradients will have similar forms except that R will change
#####################################################
function pcaObjZ(z,X,W)
    # Rezie vector of parameters into matrix
    n = size(X,1)
    k = size(W,1)
    Z = reshape(z,n,k)

    # Comptue function value
    R = Z*W - X
    f = sum(R.^2)

    # Comptue derivative with respect to each residual
    dR = R

    # Multiply by W' to get elements of gradient
    G = dR*W'
    ##HS: so G = ZWW' - XW'(i.e gradient of f w.r.t. Z)

    # Return function and gradient vector
    return (f,G[:])
end
#####################################################

#####################################################
###### LeastSquares gradient of loss w.r.t to W #####
# Gradient(f,W) = Z'ZW - Z'X
# W = inv(Z'Z) * Z'X
# or in julia:
# W = (Z'Z) \ Z'X
# HS. MM. NOTE: For ALL other differentiable loss functions of the form f(ZW), the gradients will have similar forms except that R will change
#####################################################
function pcaObjW(w,X,Z)
    # Rezie vector of parameters into matrix
    d = size(X,2)
    k = size(Z,2)
    W = reshape(w,k,d)

    # Comptue function value
    R = Z*W - X
    f = sum(R.^2)

    # Comptue derivative with respect to each residual
    dR = R

    # Multiply by Z' to get elements of gradient
    G = Z'dR
    ##HS: so G = Z'ZW - Z'X (i.e gradient of f w.r.t. W)

    # Return function and gradient vector
    return (f,G[:])
end
#####################################################







#############################################
################ robustPCA ##################
#= This is supposed to use the absolute value
of residulas but since that is not 
differenctiable we are using the multi-quadric 
approximation: 
abs(a) = sqrt(a^2+eps) where eps 
controls the accuracy of estimation typically
eps = 0.0001 

Note that for scalar t if:
f(t)  = sqrt(t^2+eps)
f'(t) = t(t^2+sps)^(-0.5) 
whre f'(t) is derivative of f w.r.t t.
=#
#############################################
function robustPCA(X,k)
    (n,d) = size(X)

    # Subtract mean
    mu = mean(X,1)
    X -= repmat(mu,n,1)

    # Initialize W and Z
    W = randn(k,d)
    Z = randn(n,k)

    eps = 0.0001
    R = Z*W - X

    f = sum(sqrt.(R.^2 .+ eps)) 
    
    funObjZ(z) = robustPcaObjZ(z,X,W,eps)
    funObjW(w) = robustPcaObjW(w,X,Z,eps)
    
    for iter in 1:50
        fOld = f

        # Update Z
        Z[:] = findMin(funObjZ,Z[:],verbose=false,maxIter=10)

        # Update W
        W[:] = findMin(funObjW,W[:],verbose=false,maxIter=10)

        R = Z*W - X

        f = sum(sqrt.(R.^2 .+ eps))

        @printf("Iteration %d, loss = %f\n",iter,f/length(X))

        if (fOld - f)/length(X) < 1e-2
            break
        end
    end

    compress(Xhat) = compress_robustPCA(Xhat,W,mu,eps)
    
    expand(Z) = expandFunc(Z,W,mu)

    return CompressModel(compress,expand,W)
end

function compress_robustPCA(Xhat,W,mu,eps)
    (t,d) = size(Xhat)
    Xcentered = Xhat - repmat(mu,t,1)
    Z = zeros(t,k)

    funObj(z) = robustPcaObjZ3(z,Xcentered,W,eps)
    Z[:] = findMin(funObj,Z[:],verbose=false)
    
    return Z
end
#############################################


#####################################################
###### robustPCS gradient of loss w.r.t to Z #####
#= 
using the multi-quadric approximation: 
abs(a) == sqrt(a^2+eps)

Note that for scalar t if:
f(t)  = sqrt(t^2+eps)
f'(t) = t(t^2+sps)^(-0.5) 
whre f'(t) is derivative of f w.r.t t.
=#
#####################################################
function robustPcaObjZ(z,X,W,eps)
    # Rezie vector of parameters into matrix
    n = size(X,1)
    k = size(W,1)
    Z = reshape(z,n,k)

    # Comptue function value
    R = Z*W - X

    f = sum(sqrt.(R.^2 .+ eps))
    
    # Comptue derivative with respect to each residual
    dR = R.*(R.^2 .+ eps).^(-0.5)

    # Multiply by W' to get elements of gradient
    G = dR*W'

    # Return function and gradient vector
    return (f,G[:])

end
#####################################################


#####################################################
###### robustPCS gradient of loss w.r.t to W #####
# using the multi-quadric approximation: 
#= 
using the multi-quadric approximation: 
abs(a) == sqrt(a^2+eps)

Note that for scalar t if:
f(t)  = sqrt(t^2+eps)
f'(t) = t(t^2+sps)^(-0.5) 
whre f'(t) is derivative of f w.r.t t.
=#
#####################################################
function robustPcaObjW(w,X,Z,eps)
    # Rezie vector of parameters into matrix
    d = size(X,2)
    k = size(Z,2)
    W = reshape(w,k,d)

    # Comptue function value
    R = Z*W - X

    f = sum(sqrt.(R.^2 .+ eps))

    # Comptue derivative with respect to each residual
    dR = R.*(R.^2 .+ eps).^(-0.5)

    # Multiply by Z' to get elements of gradient
    G = Z'dR
    ##HS: so G = Z'ZW - Z'X (i.e gradient of f w.r.t. W)

    # Return function and gradient vector
    return (f,G[:])
end
#####################################################