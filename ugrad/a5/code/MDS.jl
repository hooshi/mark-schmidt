include("misc.jl")
include("PCA.jl")
include("findMin.jl")

######################################
############ Standard MDS ############
#= This fits a statndard MDS, use with 
different D  for different models like 
ISOMAP
=#
######################################
function MDS(X)
    (n,d) = size(X)

    ######################################
    ## L2 distance giving standart MDS ##
    # Compute all distances
    D = distancesSquared(X,X)
    D = sqrt.(abs.(D))
    ######################################

    #= Initialize low-dimensional representation with PCA
    HS: use PCA just as an initial guess because MDS is very sensitive to initialization
    =#
    model = PCA(X,2)
    Z = model.compress(X)

    #HS: Begin training MDS
    funObj(z) = stress(z,D)

    Z[:] = findMin(funObj,Z[:])

    return Z
end

function stress(z,D)
    n = size(D,1)
    Z = reshape(z,n,2)

    f = 0
    G = zeros(n,2)
    for i in 1:n
        for j in i+1:n
            # Objective function
            Dz = norm(Z[i,:] - Z[j,:])
            s = D[i,j] - Dz
            f = f + (1/2)s^2

            # Gradient
            df = s
            dgi = (Z[i,:] - Z[j,:])/Dz
            dgj = (Z[j,:] - Z[i,:])/Dz
            G[i,:] -= df*dgi
            G[j,:] -= df*dgj
        end
    end
    return (f,G[:])
end


##########################################################
############# ISOMAP (Geodesic) Distance #################
#=
computes the approximate geodesic 
distance (shortest path through a 
graph  where the edges are only 
between nodes  that are k-nearest 
neighbour) between each pair of 
points, and then fits a standard 
MDS model using gradient descent
=#
##########################################################
function ISOMAP(X, k)
    (n,d) = size(X)

    ######################################
    ########## Calculating KNNs ##########
    ######################################
    #= Compute all distances:
    D[i,:] gives a row vector with
    distances btw example i and all other
    points. So  D[i,i] = 0 for all i.
    =#
    D = distancesSquared(X,X)
    D = sqrt.(abs.(D))

    #=This n*n matrix. EdgeWeights[i,:] 
    stores the edge weights between point 
    i and all other points. The weight is
    Inf (infinity) if there is no edge btw
    two points (i.e. the two points are)
    not neighbors. So EdgeWeights[i,:] has
    none Inf nodes only for its KNN
    excluding itself. So for all i
    EdgeWeights[i,i] = 0. Also this is a 
    directed graph so if there is an edge
    btw i and j, (EdgeWeights[i,j] = m) 
    there is one btw j and i too, 
       (EdgeWeights[j,i] = m also)
    =#
    EdgeWeights = zeros(n,n) .+ Inf

    for i in 1:n
        sortedDistancesIndices = sortperm(D[i,:]);
        
        #We increment k because the point itself 
        #will be one of the KNNs
        knnIndices = sortedDistancesIndices[1:(k+1)]
        
        EdgeWeights[i,knnIndices] = D[i,knnIndices]

        #=Since ISOMAP needs an undirected graph,
        if there is an edge btw i and j we need to 
        add the same edge btw j and i too:
        =# 
        for j in knnIndices
            EdgeWeights[j,i] = EdgeWeights[i,j]
            EdgeWeights[j,j] = Inf
        end

        #exclude edge to self as a KNN
        EdgeWeights[i,i] = Inf
    end


    #=Now we can use the EdgeWeights to compute 
    the shortest (weighted) distance between 
    all the points (using dijskstra algorithm). 
    =#
    shotestGraphPathDistance = zeros(n,n)
    for i in 1:n
        for j in 1:n
            shotestGraphPathDistance[i,j] = dijkstra(EdgeWeights,i,j)
        end
    end
    ##########################################################



    ##########################################################
    ########## Dealing with when Graph is disconnected #######
    # Can remove if no disconnected nodes in graph
    #=
    set Inf distances to largest distance in 
    graph to deal with cases where graph is
    disconnected. This happens for instance
    when k is too low and there are two points
    that are disconnected from eachother when 
    we connect eahc point to its KNN. 
    =#
    ##########################################################
    #Function to find the elements that are Inf
    function comparator(x)
        return x==Inf
    end
    infDistances = find(comparator, shotestGraphPathDistance);
    
    shotestGraphPathDistance[infDistances] = 0;
    maxGraphDistance = maximum(shotestGraphPathDistance);
    shotestGraphPathDistance[infDistances] = maxGraphDistance;
    ##########################################################


    #The rest is just normal MDS:

    #= Initialize low-dimensional representation with PCA
    HS: use PCA just as an initial guess because MDS is very sensitive to initialization
    =#
    model = PCA(X,2)
    Z = model.compress(X)

    #HS: Begin training MDS
    funObj(z) = stress(z,shotestGraphPathDistance)

    Z[:] = findMin(funObj,Z[:])

    return Z
end
##########################################################