# Load data
dataTable = readcsv("animals.csv")
X = float(real(dataTable[2:end,2:end]))
(n,d) = size(X)

#########################################
############### Using PCA ###############
#########################################


include("PCA.jl");
k = 50;
model = PCA(X,k);
Z = model.compress(X);

# Show scatterplot of the two principal componenets
figure(1)
clf()
plot(Z[:,1],Z[:,2],".")
for i in rand(1:n,10)
    annotate(dataTable[i+1,1],
	xy=[Z[i,1],Z[i,2]],
	xycoords="data")
end

#########################################


#########################################
############## Selecting K ##############
#########################################
#=

include("PCA.jl");
include("misc.jl");

err = 1;
k = 2;
while err > 0.2
	k = k+1;
	model = PCA(X,k);
	err = pcaReconstructionError(model,X);
end

=#
#########################################


#=

# Standardize columns
include("misc.jl")
(X,mu,sigma) = standardizeCols(X)





# Plot matrix as image
using PyPlot
figure(1)
clf()
imshow(X)

# Show scatterplot of 2 random features
j1 = rand(1:d)
j2 = rand(1:d)
figure(2)
clf()
plot(X[:,j1],X[:,j2],".")
for i in rand(1:n,10)
    annotate(dataTable[i+1,1],
	xy=[X[i,j1],X[i,j2]],
	xycoords="data")
end

=#
