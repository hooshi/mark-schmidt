include("misc.jl")

function PCA(X,k)
    (n,d) = size(X)

    # Subtract mean
    mu = mean(X,1)
    X -= repmat(mu,n,1)

    #this finds all principal componenets.
    (U,S,V) = svd(X)

    #this selects the first k principal compoenenets
    W = V[:,1:k]'

    #this takes X and returns Z using W
    compress(Xhat) = compressFunc(Xhat,W,mu)

    #this takes Z and returns X using W
    expand(Z) = expandFunc(Z,W,mu)

    error(X) = pcaReconstructionError(X, compress, expand);

    return CompressModel(compress,expand, error, W)
end

function compressFunc(Xhat,W,mu)
    (t,d) = size(Xhat)
    Xcentered = Xhat - repmat(mu,t,1)
    return Xcentered*W' # Assumes W has orthogonal rows
end

function expandFunc(Z,W,mu)
    (t,k) = size(Z)
    return Z*W + repmat(mu,t,1)
end

###HS: returns the (variance of errors)/(variance of xij)
function pcaReconstructionError(X, compress, expand)
    Z = compress(X);
    Xhat = expand(Z);

    varianceOfErrors = vecnorm(Xhat - X);
    varianceOfxij = vecnorm(X);

    error = varianceOfErrors/varianceOfxij;

    @printf("This model can explain %d percent of the variance in the data\r\n\n", (1-error)*100)

    @printf("calculated error: \r\n")

    return error;
end