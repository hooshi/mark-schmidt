include("misc.jl")
include("findMin.jl")

function logReg(X,y)

    (n,d) = size(X)

    # Initial guess
    w = zeros(d,1)

    # Function we're going to minimize (and that computes gradient)
    funObj(w) = logisticObj(w,X,y)

    # Solve least squares problem
    w = findMin(funObj,w,derivativeCheck=true)

    # Make linear prediction function
    predict(Xhat) = sign.(Xhat*w)

    # Return model
    return LinearModel(predict,w)
end

function logisticObj(w,X,y)
    yXw = y.*(X*w)
    f = sum(log.(1 + exp.(-yXw)))
    g = -X'*(y./(1+exp.(yXw)))
    return (f,g)
end

##
## Multi-class one-vs-all version (assumes y_i in {1,2,...,k})
##
function logRegOnevsAll(X,y)
    (n,d) = size(X)
    k = maximum(y)

    # Each column of 'w' will be a logistic regression classifier
    W = zeros(d,k)

    for c in 1:k
	yc = ones(n,1) # Treat class 'c' as +1
	yc[y .!= c] = -1 # Treat other classes as -1

	# Each binary objective has the same features but different lables
	funObj(w) = logisticObj(w,X,yc)

	W[:,c] = findMin(funObj,W[:,c],verbose=false)
    end

    # Make linear prediction function
    predict(Xhat) = mapslices(indmax,Xhat*W,2)

    return LinearModel(predict,W)
end

##
## Multi-class softmax version 
##
function logRegSoftmax(X,y)
    (n,d) = size(X)
    k = maximum(y)

    ##
    ## Find the minimizer of the softmax objective function
    ##
    W = zeros(d*k)
    funObj3(input) = softmaxObj(input,X,y,k)
    W = findMin(funObj3,W,maxIter=100,derivativeCheck=true,verbose=true)
    W = reshape(W, (d,k))
    
    ##
    ## Make linear prediction function
    ##
    predict(Xhat) = mapslices(indmax,Xhat*W,2)

    return LinearModel(predict,W)
end

##
## Softmax objective function and its derivative 
##
function softmaxObj(w,X,y,k)
    (n,d) = size(X)
    w = copy(w)
    w = reshape(w, (d,k))

    ##
    ## Find the intermediate variables
    ##
    beta = X*w
    exp_beta = exp.( beta  )
    alpha = sum(exp_beta,2)
    probabilities = zeros(n,k)
    eyematrix = zeros(n,k)
    for i in 1:n
        for c in 1:k
            probabilities[i,c] = exp_beta[i,c] / alpha[i]
            if(y[i] == c)  eyematrix[i,c] = 1  end
        end
    end

    ##
    ## Find the objective function
    ## 
    ff = 0
    for i in 1:n
         ff = ff - dot(w[:,y[i]], X[i,:])
    end
    ff = ff + sum( log.(alpha) )

    ##
    ## Find the gradient
    ##
    gg = transpose(X) * ( probabilities - eyematrix )
    gg = reshape(gg,(d*k,1))    
    return (ff,gg)
end
