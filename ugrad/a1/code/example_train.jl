#!/home/hooshi/bin/julia

# Load X and y variable
using JLD
X_ = load("citiesSmall.jld","X")
y_ = load("citiesSmall.jld","y")
# Xtest = load("citiesSmall.jld","Xtest")
# ytest = load("citiesSmall.jld","ytest")
n_ = size(X_,1)

Xtest = X_[1:Int(n_/2),:]
X = X_[Int(n_/2)+1:end,:]
ytest = y_[1:Int(n_/2)]
y = y_[Int(n_/2)+1:end]

n = size(X,1)
ntest = size(Xtest,1)

# Maximum depth we will plot
maxDepth = 20

# include("decisionTree.jl")
# ourerror=zeros(maxDepth)
# infoerror= zeros(maxDepth)
# for depth in 1:maxDepth
#     model = decisionTree(X,y,depth)

#     yhat = model.predict(X)
#     trainError = sum(yhat .!= y)/n
#     @printf("Training error with depth-%d accuracy-based decision tree: %.2f\n",depth,trainError)
#     ourerror[depth] = trainError
# end

@printf("Now let's try infogain instead of accuracy...\n")

include("decisionTree_infoGain.jl")
testerror=zeros(maxDepth)
trainerror= zeros(maxDepth)
for depth in 1:maxDepth
    model = decisionTree_infoGain(X,y,depth)

    yhat = model.predict(X)
    trainError = sum(yhat .!= y)/n
    yhat = model.predict(Xtest)
    testError = sum(yhat .!= ytest)/ntest
    @printf("Training error with depth-%d infogain-based decision tree: %.2f\n",depth,trainError)
    trainerror[depth] = trainError
    testerror[depth] = testError
end

# PyPlot.plot(1:maxDepth, ourerror,linewidth=2,marker="o",markersize=3,label="Our tree")
# PyPlot.plot(1:maxDepth, infoerror,linewidth=2,marker="s",markersize=3,label="Infogain")
# PyPlot.xlabel("depth")
# PyPlot.ylabel("training error")
# PyPlot.legend()
# PyPlot.savefig("../tex/img/infogain.pdf",bbox_inches="tight")
# PyPlot.show()

using PyPlot

plot(1:maxDepth, trainerror,linewidth=2,marker="o",markersize=3,label="Training")
plot(1:maxDepth, testerror,linewidth=2,marker="s",markersize=3,label="Test")
xlabel("depth")
ylabel("error")
legend()
savefig("../tex/img/infogain4.pdf",bbox_inches="tight")
show()
