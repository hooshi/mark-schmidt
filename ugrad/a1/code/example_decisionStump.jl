#!/home/hooshi/bin/julia

using JLD

include("plot2Dclassifier.jl")
include("majorityPredictor.jl")
include("decisionStump.jl")

##-----------------------------------------------------------------------
##                         Load data
##-----------------------------------------------------------------------

# Load X and y variable
X = load("citiesSmall.jld","X")
y = load("citiesSmall.jld","y")

# Compute number of objects and number of features
(n,d) = size(X)

##-----------------------------------------------------------------------
##                        Majority Predictor
##-----------------------------------------------------------------------

# Fit majority predictor and compute error
model = majorityPredictor(X,y)

# Evaluate training error
yhat = model.predict(X)
trainError = sum(yhat .!= y)/n
@printf("Error with majority predictor: %.2f\n",trainError);

##-----------------------------------------------------------------------
##                        Decision Stump Model 
##-----------------------------------------------------------------------

#
# Fit decision stump classifier that uses equalities
#
model = decisionStumpEquality(X,y)

# Evaluate training error
yhat = model.predict(X)
trainError = sum(yhat .!= y)/n
@printf("Error with equality-rule decision stump: %.2f\n\n",trainError);

# Plot classifier
plot2Dclassifier(X,y,model)

#
# Fit decision stump classifier that uses INequalities
#
model = decisionStump(X,y)

# Evaluate training error
yhat = model.predict(X)
trainError = sum(yhat .!= y)/n
@printf("Error with inequality-rule decision stump: %.2f\n\n",trainError);

# Plot classifier
plot2Dclassifier(X,y,model)

show()
