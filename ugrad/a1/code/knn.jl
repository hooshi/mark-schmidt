#!/home/hooshi/bin/julia

## -------------------------------------------------------------
##                        Includes
## -------------------------------------------------------------
include("misc.jl") # Includes GenericModel typedef

## -------------------------------------------------------------
##                        Functions 
## -------------------------------------------------------------

"""
knn_predict() 

Completed as part of the assignment.
"""
function knn_predict(Xhat,X,y,k)
  (n,d) = size(X)
  (t,d) = size(Xhat)
  k = min(n,k) # To save you some debuggin

  output = zeros(t);
 
  for j in 1:t	
	distances = zeros(n);
	for i in 1:n
	   distances[i] = sqrt(sum(abs2.(X[i:i,:]-Xhat[j:j,:])));
	end
	sortedDistancesIndices = sortperm(distances);
	output[j] = mode(y[sortedDistancesIndices[1:k]]);
  end

  return output
end

"""
knn()

Implementation of k-nearest neighbour classifier
"""
function knn(X,y,k)
  predict(Xhat) = knn_predict(Xhat,X,y,k)
  return GenericModel(predict)
end


"""
cknn()

Implementation of condensed k-nearest neighbour classifier
"""
function cknn(X,y,k)
	(n,d) = size(X)
	Xcond = X[1,:]'
	ycond = [y[1]]
	for i in 2:n
    		yhat = knn_predict(X[i:i,:],Xcond,ycond,k)
    		if y[i] != yhat[1]
			Xcond = vcat(Xcond,X[i,:]')
			push!(ycond,y[i])
    		end
	end
	@printf("Size of Xcond is: %d\n",size(Xcond,1))
	predict(Xhat) = knn_predict(Xhat,Xcond,ycond,k)
	return GenericModel(predict)
end
