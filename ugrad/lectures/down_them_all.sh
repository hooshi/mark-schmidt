#!/bin/bash

n_lectures=34
for ii in $(seq 1 ${n_lectures}); do
    wget "http://www.cs.ubc.ca/~schmidtm/Courses/340-F17/L${ii}.pdf"
done
