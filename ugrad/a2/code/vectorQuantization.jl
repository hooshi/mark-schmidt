include("kMeans.jl")

"""
Vector quantization. Given an image (a n_row*n_col*3 matrix),
and b (the number of clusters would be 2^b), returns a n_row*n_col matrix
containing the cluster index of each pixel, and the value of the clusters.
"""
function quantizeImage(img, bb)

    ## Get the sizes
    n_col = size(img,2)
    n_row = size(img,1)
    n_feature = size(img, 3)
    n_sample = n_col*n_row

    ## Ignore alpha channel
    if(n_feature == 4) img=img[:,:,1:end-1]  end
    assert( (n_feature == 3) || (n_feature == 1) )

    ## Resize the image 
    XX = reshape(img, :, n_feature)
    @printf("img size is: %s, XX size is %s \n", size(img), size(XX))

    ## Do the clustering
    n_cluster = 2 ^ Int(bb)
    partitioning = kMeans(XX, n_cluster, doPlot=true)
    
    ## Get the clusters and the lables
    clusters = partitioning.W
    labels = partitioning.y
    labels = reshape(labels, n_row, n_col)

    return (labels,clusters)

    @printf("%s", size(clusters))
    
end ## End of quantizeImage()


""" Convert quantized information to an image """
function deQuantizeImage(labels, clusters)
    n_channel = size(clusters,2)
    @printf("number of cluster colors: %d \n", n_channel)
    
    n_row = size(labels, 1)
    n_col = size(labels, 2)

    img = zeros(n_row, n_col, n_channel)

    if(n_channel > 1)
        img = clusters[ labels, : ]
    else
        img = clusters[ labels ]
    end

    return img
end ## End of quantizeImage()
