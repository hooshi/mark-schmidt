#!/usr/bin/env julia

# Load X and y variable
using PyCall
@pyimport matplotlib.pyplot as plt
import JLD
include("leastSquares.jl")

data = JLD.load("basisData.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

# Fit a least squares model
modeltypename =  readParam(ARGS, "-type", String, "nobias");
model = leastSquaresFactory(X,y, modeltypename)

# Evaluate training error
yhat = model.predict(X)
trainError = mean((yhat - y).^2)
@printf("Squared train Error with least squares: %.3f\n",trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean((yhat - ytest).^2)
@printf("Squared test Error with least squares: %.3f\n",testError)

# Plot model
plt.figure()
plt.plot(X,y,"b.",label="Training set")
plt.plot(Xtest,ytest,"g.", label="Test set")
Xhat = minimum(X):.1:maximum(X)
yhat = model.predict(Xhat)
plt.plot(Xhat,yhat,"r",label="Fitted curve")
plt.legend()

plt.xlabel("x")
plt.ylabel("y")
plt.savefig("fourier-fitting-2.pdf", bbox_inches="tight")

plt.show()

