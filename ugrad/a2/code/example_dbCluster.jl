#!/usr/bin/env julia

using JLD
include("misc.jl");
include("dbCluster.jl");
include("clustering2Dplot.jl");

# Load data
X = load("clusterData2.jld","X");

# Density-based Clustering
radius = readParam(ARGS, "-r", Int, 1);
minPts = readParam(ARGS, "-m", Int, 2);
@printf("radius is %d \n", radius);
@printf("minPts is %d \n", minPts);


y = dbCluster(X,radius,minPts,doPlot=true);

clustering2Dplot(X,y);

plt.show();
