#!/usr/bin/env julia

# Load X and y variable
using PyCall
@pyimport matplotlib.pyplot as plt
import JLD
include("leastSquares.jl")

data = JLD.load("basisData.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

# Fit a least squares model
Ls = [0.620,0.621,0.622,0.623,0.624,0.625,0.626,0.627,0.628, 0.629, 0.630, 0.631, 0.632, 0.633,0.634,0.635,0.636,0.637,0.638,0.639,0.640,0.641,0.642,0.643]
trainError = zeros(size(Ls))
testError = zeros(size(Ls))

for i=1:size(Ls,1)

model = leastSquaresFourier(X,y,3,1,Ls[i])

yhat = model.predict(X)
trainError[i] = mean((yhat - y).^2)

yhat = model.predict(Xtest)
testError[i] = mean((yhat - ytest).^2)

end


plt.figure()
plt.plot(Ls,trainError,"b-o",label="Training Error")
plt.plot(Ls,testError,"g-^",label="Test Error")
plt.legend()
plt.xlabel("\$L\$")
plt.ylabel("\$\|y-\\hat{y}\|\$")
plt.savefig("fourier-fitting-1.pdf", bbox_inches="tight")
    
plt.show()

