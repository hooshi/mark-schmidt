# Load data
using JLD

#=
X = load("clusterData2.jld","X")


# K-means clustering
k = 4
include("kMeans.jl")

model = kMedians(X,k,doPlot=true)
y = model.predict(X)

include("clustering2Dplot.jl")
clustering2Dplot(X,y,model.W)
=#





##################################################
########## Running KMeans 50 times ###############
##################################################
#=
X = load("clusterData2.jld","X")

include("kMeans.jl")

k = 4

error = Inf;
finalModel = [];
for i in 1:50
	model = kMeans(X,k,doPlot=false);
	
	tempEr = kMeansError(X, model.y, model.W);
	@printf("k-means Error is: %f\n",tempEr);

	if tempEr < error
		error = tempEr;
		finalModel = model;
	end
end

y = finalModel.predict(X)

include("clustering2Dplot.jl")
clustering2Dplot(X,finalModel.y,finalModel.W)

@printf("Final k-means Error is: %f\n",error);
=#
##################################################





##################################################
########## Running KMedians 50 times ###############
##################################################
#=
X = load("clusterData2.jld","X")

include("kMeans.jl")

k = 4

error = Inf;
finalModel = [];
for i in 1:50
	model = kMedians(X,k,doPlot=false);
	
	tempEr = kMediansError(X, model.y, model.W);
	@printf("k-medians Error is: %f\n",tempEr);

	if tempEr < error
		error = tempEr;
		finalModel = model;
	end
end

y = finalModel.predict(X)

include("clustering2Dplot.jl")
clustering2Dplot(X,finalModel.y,finalModel.W)

@printf("Final k-medians Error is: %f\n",error);
=#
##################################################





##################################################
########## Choosing value of K ###############
##################################################
X = load("clusterData2.jld","X")

include("kMeans.jl")

finalErrors = Array{Float64}(10);

for k in 1:10
    
    error = Inf;
    finalModel = [];
    
    for i in 1:50
	model = kMedians(X,k,doPlot=false);
	
	tempEr = kMediansError(X, model.y, model.W);
	@printf("k-medians Error is: %f\n",tempEr);

	if tempEr < error
	    error = tempEr;
	    finalModel = model;
	end
    end

    tempEr = kMediansError(X, finalModel.y, finalModel.W);
    @printf("k-medians Error for k=%d is: %f\n", k, tempEr);
    finalErrors[k] = tempEr;
end

using PyPlot
plot(1:10,finalErrors ,linewidth=2,marker="s",markersize=3,label="Test")
xlabel("K values")
ylabel("kMediansError")
title("kMedianError for best out of 50 tries for 10 values of k")
show()
##################################################
