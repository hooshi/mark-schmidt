\section{Discrete and Gaussian Variables}

\subsection{MLE for General Discrete Distribution}

Consider a density estimation task, where we have two variables ($d=2$) that can each take one of $k$ discrete values. For example, we could have
\[
X = \mat{1 & 3\\4 & 2\\$k$ & 3\\1 & $k-1$}.
\]
The likelihood for example $x^i$ under a general discrete distribution would be
\[
p(x^i_1, x^i_2 \cond \Theta) = \theta_{x_1^i,x_2^i},
\]
where $\theta_{c_1,c_2}$ gives the probability of $x_1$ being in state $c_1$ and $x_2$ being in state $c_2$, for all the $k^2$ combinations of the two variables. In order for this to define a valid probability, we need all elements $\theta_{c_1,c_2}$ to be non-negative and they must sum to one, $\sum_{c_1=1}^k\sum_{c_2=1}^k \theta_{c_1,c_2} = 1$.
\enum{
\item Given $n$ training examples, \blu{derive the MLE for the $k^2$ elements of $\Theta$}.
\ans{We have
\alignStar{
p(X|\Theta) &= \prod_{i=1}^n p(x^i|\Theta)= \prod_{i=1}^n\theta_{x^i}= \prod_{c \in [k]^2} \theta_c^{\sum_{i=1}^nI[x^i=c]}\\&= \prod_{c \in [k]^2} \theta_c^{n_c}
}
so we want 
\alignStar{\Theta^* &\in \argmax_\Theta \prod_{c \in [k]^2} \theta_c^{n_c}\\
&\equiv \argmax_\Theta \log\left(\prod_{c \in [k]^2} \theta_c^{n_c}\right)\\
&= \argmax_\Theta \sum_{c \in [k]^2}\log \theta_c^{n_c}\\
&= \argmax_\Theta \sum_{c \in [k]^2}n_c\log \theta_c\\
}
subject to constraints $\sum_{c \in [k]^2}\theta_c = 1$ and $\sum_{c \in [k]^2}n_c = n$. Letting $w$ contain all the values of $\theta_c$, we form the Lagrangian expressing these constraints as by letting $A$ be a row vector in $\mathbb{R}^k$ containing all ones and $b=1$.We then have $$L(w,v) = -NLL(w) + v^T(Aw-b)$$ so $$\nabla_wL = -\nabla_wNLL(w) + A^Tv = 0$$ and $$\nabla_v L = Aw-b=0$$ for scalar-valued $v$. This gives element wise equations $$v = \frac{\partial}{\partial \theta_c} \sum_{c \in [k]^2} n_c\log(\theta_c) = \frac{n_c}{\theta_c}$$ for all $c$. Multiplying by each by $\theta_c$ and summing over all $c$ gives $$\sum_{c \in [k]^2}\theta_cv = v\sum_{c \in [k]^2}\theta_c=v =n= \sum_{c \in [k]^2}n_c$$
so $\theta_c = \frac{n_c}{n}$ for all $c$.
}
\item If we had separate parameter $\theta_{c_1}$ and $\theta_{c_2}$ for each variables, a reasonable choice of a prior would be a product of Dirichlet distributions,
\[
p(\theta_{c_1},\theta_{c_2}) \propto \theta_{c_1}^{\alpha_{c_1} - 1}\theta_{c_2}^{\alpha_{c_2} - 1}.
\]
For the general discrete distribution, a prior encoding the same assumptions  would be
\[
p(\theta_{c_1,c_2}) \propto \theta_{c_1,c_2}^{\alpha_{c_1} + \alpha_{c_2} - 2}.
\]
\blu{Derive the MAP estimate under this prior} (assuming we use $k^2$ variables to parameterize $\Theta$)
\ans{
The MAP estimate is given by
\alignStar{
\Theta^* &\in \argmax_\Theta 
\left\lbrace p(X|\Theta)p(\Theta)\right\rbrace\\
&\equiv \argmax_\Theta \left\lbrace \log p(X|\Theta) + \log p (\Theta)\right\rbrace\\
&= \argmax_\Theta \left\lbrace \sum_{c \in [k]^2} n_c \log \theta_c + \log \prod_{c \in [k[^2}\theta_c^{\alpha_c - 2}\right\rbrace\\ 
&= \argmax_\Theta \left\lbrace \sum_{c \in [k]^2} n_c \log \theta_c + \sum_{c \in [k[^2}\log \theta_c^{\alpha_c - 2}\right\rbrace\\ 
&= \argmax_\Theta \left\lbrace \sum_{c \in [k]^2} (n_c + \alpha_c - 2) \log \theta_c \right\rbrace\\ 
}
where $\alpha_c = \alpha_{c_1} + \alpha_{c_2}$. Similarly to the MLE case above, the Lagrange multiplier gives element wise equations $$n_c + \alpha_c - 2 = v\theta_c$$ for all $c$. Summing over all $c$ gives
$$\sum_{c'\in[k]^2}n_{c'}+\alpha_{c'}-2 = \sum_{c'\in[k]^2}v\theta_{c'}=v\sum_{c'\in[k]^2}\theta_{c'} = v$$so
$$\theta_c = \frac{n_c+\alpha_c-2}{\sum_{c'\in[k]^2}n_{c'}+\alpha_{c'}-2}$$ for all $c$.
}
\item We often use discrete distributions as parts of more-complicated distributions (like mixture models and graphical models). In these cases we often need to fit a weighted NLL for the form
\[
f(\Theta) = -\sum_{i=1}^n v_i \log p(x_1^i, x_2^i \cond \Theta),
\]
with $n$ non-negative weights. \blu{What is the MLE for the $k^2$ elements of $\Theta$ under this weighting.}
\ans{Let $p(x^i|\Theta) = \theta_{x^i}$, so $f(\Theta) = -\sum_{i=1}^n v_i \log \theta_{x^i}$ and $$\Theta^* \in \argmax_\Theta \left\lbrace\sum_{i=1}^nv_i\log\theta_{x^i}\right\rbrace$$
Lagrange gives element-wise equations:
\alignStar{
\frac{\partial}{\partial \theta_c} f(\Theta) &= \frac{\partial}{\partial\theta_c}\left(-\sum_{i=1}^nv_i\log\theta_{x^i}\right)+v \\
&= -\sum_{x^i=c}\frac{v_i}{\theta_c} + v\\
&= -\frac{1}{\theta_c}\sum_{x^i=c}v_i + v = 0
}
so $\theta_c v = \sum_{x^i=c}v_i $ for all $c$. Summing over all $c$ gives
$$\sum_{c\in[k]^2}\sum_{x^i=c}v_i = \sum_{i=1}^nv_i=\sum_{c\in[k]^2}v\theta_c = v\sum_{c\in[k]^2}\theta_c=v$$ so 
$$\frac{\sum_{x^i=c}v_i}{\sum_{i=1}^nv_i}$$
}
}



\newpage
\subsection{Gaussian Self-Conjugacy and Posterior Convergence Rate}

Consider $n$ IID samples $x^i$ distributed according to a Gaussian with mean $\mu$ and covariance $\sigma^2 I$,
\[
x^i \sim \mathcal{N}(\mu, \sigma^2 I).
\]
Assume that $\mu$ itself is distributed according to a Gaussian
\[
\mu \sim \mathcal{N}(\mu_0,\Sigma_0),
\]
with mean $\mu_0$ and (positive-definite) covariance $\Sigma_0$. In this setting, the posterior for $\mu$ also follows a Gaussian distribution.\footnote{We say that the Gaussian distribution is the `conjugate prior' for the Gaussian mean parameter (we'll formally discuss conjugate priors later in the course). Another reason the Gaussian distribution is important is that is the only (non-trivial) continuous distribution that has this ``self-conjugacy'' property.}

\enum{
\item \blu{Derive the form of the posterior distribution, $p(\mu\cond X, \sigma^2, \mu_0, \Sigma_0)$.}\\
\ans{By Baye's rule, we have:
\alignStar{
p(\mu|X, \sigma^2, \mu_0, \Sigma_0) &\propto p(\mu|\mu_0, \Sigma_o)p(X|\mu, \sigma^2)\\
&= p(\mu|\mu_0, \Sigma_o)\prod_{i=1}^np(x^i|\mu, \sigma^2)
}
so
$$\log p(\mu|X, \sigma^2, \mu_0, \Sigma_0) = \log p(\mu|\mu_0, \Sigma_o) + \sum_{i=1}^n \log p(x^i|\mu, \sigma^2) + \text{const.}$$
Expanding in terms of the pdfs gives 
\alignStar{
\log p(\mu|\mu_0, \Sigma_o) &= -\frac{1}{2}(\mu-\mu_0)^T\Sigma_0^{-1}(\mu-\mu_0) + \text{const.}\\
&= -\half \left( \mu^T\Sigma_0^{-1}\mu - 2\mu^T\Sigma_0^{-1}\mu_0 + \mu_0^T\Sigma_0^{-1}\mu_0\right) + \text{const.}
}
and
\alignStar{
\sum_{i=1}^n \log p(x^i|\mu, \sigma^2) &= -\half\sum_{i=1}^n(x^i-\mu)^T(\sigma^2I)^{-1}(x^i-\mu) + \text{const.}\\
&= -\frac{1}{2\sigma^2}\left[\sum_{i=1}^n (x^i)^T(x^i) - 2 \sum_{i=1}^n\mu^Tx^i + n\mu^T\mu \right] + \text{const.}
} 
Adding these together and ignoring all terms that are constant with respect to $\mu$, we get
\alignStar{
\log p(\mu|X, \sigma^2, \mu_0, \Sigma_0) = -\half \mu^T\left(\Sigma_0^{-1} + \frac{n}{\sigma^2}I\right)\mu + \mu^T\left(\Sigma_0^{-1}\mu_0 + \frac{1}{\sigma^2}\sum_{i=1}^nx^i\right) + \text{const.}\\
}
We complete the square, giving
\alignStar{
\log p(\mu|X, \sigma^2, \mu_0, \Sigma_0) = -\half(\mu-\mu_n)^T\Sigma_n^{-1}(\mu-\mu_n) + \text{const.}\\
}
with 
$$\mu_n = \left(\Sigma_0^{-1}+\frac{n}{\sigma^2}I\right)^{-1}\left(\Sigma_0^{-1}\mu_0+\frac{1}{\sigma^2}\sum_{i=1}^nx^i\right)$$
and
$$\Sigma_n = \left(\Sigma_0^{-1} + \frac{n}{\sigma^2}I\right)^{-1}$$. Since this is proportional to a Gaussian, we have that 
$$\mu|X,\sigma^2, \mu_0, \Sigma_0 \sim \mathcal{N}(\mu_n, \Sigma_n)$$
}
\item To measure the speed at which the posterior converges to $\mu$, we can consider the variance of the posterior. Consider a version of the previous question where $d=1$: \blu{how many examples $n$ (in $O$-notation) do we need before we have $|\Sigma| < \epsilon$} (for the posterior variance $\Sigma$).
\ans{In the $d=1$ case we have $$\Sigma_n = \frac{1}{\left( \frac{1}{\sigma_0^2} + \frac{n}{\sigma^2}\right)} < \epsilon$$ for $n$ such that 
$$n > \sigma^2\left(\frac{1}{\epsilon}-\frac{1}{\sigma_0^2}\right)$$ 
so $n=O(1/\epsilon)$.
}}


\newpage
\subsection{Generative Classifiers with Gaussian Assumption}

Consider the 3-class classification dataset in this image:
%% \centerfig{.4}{sample}
In this dataset, we have 2 features and each colour represents one of the classes. Note that the classes are highly-structured: the colours each roughly follow a Gausian distribution plus some noisy samples.

Since we have an idea of what the features look like for each class, we might consider classifying  inputs $x$ using a \emph{generative classifier}. In particular, we are going to use Bayes rule to write
\[
p(y^i=c\cond x^i,\Theta) = \frac{p(x^i\cond y^i=c, \Theta) \cdot p(y^i=c\cond\Theta)}{p(x^i\cond\Theta)},
\]
where $\Theta$ represents the parameters of our model. To classify a new example $\tilde{x}^i$, generative classifiers would use
\[
\hat{y}^i = \argmax_{y \in \{1,2,\dots,k\}} p(\tilde{x}^i\cond y^i=c,\Theta)p(y^i=c\cond\Theta),
\]
where in our case the total number of classes $k$ is $3$.\footnote{The denominator $p(\tilde{x}^i\cond\Theta)$ is irrelevant to the classification since it is the same for all $y$.}
% and $\theta_c$ is the set of parameters associated class $c$.
Modeling $p(y^i=c\cond\Theta)$ is easy: we can just use a $k$-state categorical distribution,
\[
p(y^i = c \cond \Theta) = \theta_c,
\]
where $\theta_c$ is a single parameter for class $c$. The maximum likelihood estimate of $\theta_c$ is given by $n_c/n$, the number of times we have $y^i = c$ (which we've called $n_c$) divided by the total number of data points $n$.

Modeling $p(x^i \cond y^i =c, \Theta)$ is the hard part: we need to know the \emph{probability of seeing the feature vector $x^i$ given that we are in class $c$}. This corresponds to solving a density estimation problem for each of the $k$ possible classes. 
To make the density estimation problem tractable, we'll assume that the distribution of $x^i$ given that $y^i=c$ is given by a $\mathcal{N}(\mu_c,\Sigma_c)$ Gaussian distribution for a class-specific $\mu_c$ and $\Sigma_c$,
\[
p(x^i \cond y^i=c, \Theta) = \frac{1}{(2\pi)^{\frac{d}{2}}|\Sigma_c|^{\half}}\exp\left(-\half (x^i-\mu_c)^T\Sigma_c^{-1}(x^i-\mu_c)\right).
\]
Since we are distinguishing between the probability under $k$ different Gaussians to make our classification, this is called \emph{Gaussian discriminant analysis} (GDA). In the special case where we have a constant $\Sigma_c = \Sigma$ across all classes it is known as \emph{linear discriminant analysis} (LDA) since it leads to a linear classifier between any two classes (while the region of space assigned to each class forms a convex polyhedron as in $k$-means clustering and softmax classification). Another common restriction on the $\Sigma_c$ is that they are diagonal matrices, since this only requires $O(d)$ parameters instead of $O(d^2)$ (corresponding to assuming that the features are independent univariate Gaussians given the class label).
Given a dataset $\mathcal{D}=\{(x^i, y^i)\}_{i=1}^n$, where $x^i\in\R^d$ and $y^i\in\{1,\ldots,k\}$, the maximum likelihood estimate (MLE) for the $\mu_c$ and $\Sigma_c$ in the GDA model is the solution to
\[
\argmax_{\mu_1,\mu_2,\dots,\mu_k,\Sigma_1,\Sigma_2,\dots,\Sigma_k} \prod_{i=1}^n p(x^i \cond y^i, \mu_{y^i},\Sigma_{y^i}).
\]
This means that the negative log-likelihood will be  equal to
\alignStar{
- \log p(X\cond y,\Theta) & = -\sum_{i=1}^n \log p(x^i , y^i \cond \mu_{y^i},\Sigma_{y^i})\\
& = \sum_{i=1}^n \frac{1}{2}(x^i - \mu_{y^i})^T\Sigma_{y^i}^{-1}(x^i - \mu_{y^i}) + \half\sum_{i=1}^n \log|\Sigma_{y^i}| + \text{const.}
}
In class we derived the MLE for this model under the assumption that we use full covariance matrices and that each class has its own covariance.
\enum{
\item \blu{Derive the MLE for the GDA model under the assumption of \emph{common diagonal covariance} matrices}, $\Sigma_c = D$ ($d$ parameters). (Each class will have its own mean $\mu_c$.)
\ans{We have 
\alignStar{
- \log p(X\cond y,\Theta) & = -\sum_{i=1}^n \log p(x^i , y^i \cond \mu_{y^i},\Sigma_{y^i})\\
& = \sum_{i=1}^n \frac{1}{2}(x^i - \mu_{y^i})^T\Sigma_{y^i}^{-1}(x^i - \mu_{y^i}) + \half\sum_{i=1}^n \log|\Sigma_{y^i}| + \text{const.}\\
& = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T\Sigma_{c}^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|\Sigma_{c}|\right) + \text{const.}\\
& = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T D^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|D|\right) + \text{const.}
}
This is strongly convex with respect to $\mu_c$; taking the gradient and setting to zero yields $$\mu_c = \frac{1}{n_c}\sum_{i \in y_c} x^i$$ for each $c$. For the calculation of $D_{MLE}$, let $\hat{x^i} = x_i - \mu_{y^i}$. Note that $\log |D| = log \prod_{i=1}^d D_j$ where $D_j = D{jj}$ (since $D$ is diagonal). Further, note $$v^TD^{-1}v = \sum_{j=1}^d D_j^{-1}v_j^2$$ in general. Thus
\alignStar{
- \log p(X\cond y,\Theta) & = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T D^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|D|\right) + \text{const.}\\
&= \sum_{c=1}^k \left(\frac{1}{2}\sum_{i \in y_c} (\hat{x^i})^T D^{-1}(\hat{x^i}) + \frac{n_c}{2} \log|D|\right) + \text{const.}\\
&= \sum_{c=1}^k \left(\frac{1}{2}\sum_{i \in y_c} \sum_{j=1}^d D_j^{-1}(\hat{x^i_j})^2 + \frac{n_c}{2} \sum_{j=1}^d\log|D_j|\right) + \text{const.}\\
}
so the gradient entries are given by
\alignStar{
\frac{\partial}{\partial D_m}(- \log p(X\cond y,\Theta)) &= \frac{\partial}{\partial D_m} \left[\sum_{c=1}^k \left(\frac{1}{2}\sum_{i \in y_c} \sum_{j=1}^d D_j^{-1}(\hat{x^i_j})^2 + \frac{n_c}{2} \sum_{j=1}^d\log|D_j|\right) + \text{const.}\right]\\
&= \sum_{c=1}^k \left(\frac{1}{2}\sum_{i \in y_c} \frac{\partial}{\partial D_m}\sum_{j=1}^d D_j^{-1}(\hat{x^i_j})^2 + \frac{n_c}{2}\frac{\partial}{\partial D_m} \sum_{j=1}^d\log|D_j|\right)\\
&= \sum_{c=1}^k \left(\frac{1}{2}\sum_{i \in y_c}  (-D_m^{-2})(\hat{x^i_m})^2 + \frac{n_c}{2}\frac{1}{D_m}\right)\\
&= \sum_{c=1}^k \left(-\frac{1}{2D_m^2}\sum_{i \in y_c}  (\hat{x^i_m})^2 + \frac{n_c}{2d_m}\right)\\
}
Define $S_c = \frac{1}{n_c}\sum_{i \in y_c} (x^i - \mu_c)(x^i - \mu_c)^T = \frac{1}{n_c}\sum_{i \in y_c} (\hat{x^i})(\hat{x^i})^T$. Then $(n_cS_c)_{p,p} = \sum_{i \in y_c} (\hat{x^i_p})^2$, so we have
\alignStar{
\frac{\partial}{\partial D_m}(- \log p(X\cond y,\Theta)) &= \sum_{c=1}^k \left(-\frac{1}{2D_m^2}\sum_{i \in y_c}  (\hat{x^i_m})^2 + \frac{n_c}{2d_m}\right)\\
&= \sum_{c=1}^k \left(-\frac{n_c}{2D_m^2}(S_c)_{m,m} + \frac{n_c}{2d_m}\right)\\
}
Setting this to zero gives:
$$\sum_{c=1}^k\frac{n_c}{2D_m^2}(S_c)_{m,m} = \sum_{c=1}^k\frac{n_c}{2D_m}$$
so
$$\frac{1}{D_m}\sum_{c=1}^kn_c(S_c)_{m,m} = \sum_{c=1}^kn_c = n$$
and 
$$D_m = \frac{1}{n}\sum_{c=1}^kn_c(S_c)_{m,m} $$
(where, as a reminder, $D_m = D_{m,m}$).
}
\item \blu{Derive the MLE for the GDA model under the assumption of \emph{individual scale-identity} matrices}, $\Sigma_c = \sigma_c^2 I$ ($k$ parameters).
\ans{We have 
\alignStar{
- \log p(X\cond y,\Theta) & = -\sum_{i=1}^n \log p(x^i , y^i \cond \mu_{y^i},\Sigma_{y^i})\\
& = \sum_{i=1}^n \frac{1}{2}(x^i - \mu_{y^i})^T\Sigma_{y^i}^{-1}(x^i - \mu_{y^i}) + \half\sum_{i=1}^n \log|\Sigma_{y^i}| + \text{const.}\\
& = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T\Sigma_{c}^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|\Sigma_{c}|\right) + \text{const.}\\
& = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T (\sigma_c^2I)^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|\sigma_c^2I|\right) + \text{const.}
}
By a similar argument to 1.3.1, $$\mu_c = \frac{1}{n_c}\sum_{i \in y_c} x^i$$ for each $c$.To find the $\sigma_C^2$ values, note that $\sum_{i \in y_c} \log|\sigma_c^2I| = \sum_{i \in y_c} \log \prod_{j=1}^d \sigma_c^2 = 2n_cd\log \sigma_c$. Thus we have
\alignStar{
- \log p(X\cond y,\Theta) 
& = \sum_{c=1}^k \left(\sum_{i \in y_c} \frac{1}{2}(x^i - \mu_{c})^T (\sigma_c^2I)^{-1}(x^i - \mu_{c}) + \half\sum_{i \in y_c} \log|\sigma_c^2I|\right) + \text{const.} \\
&= \sum_{c=1}^k \left(\frac{1}{2\sigma_c^2}\sum_{i \in y_c} (x^i - \mu_{c})^T(x^i - \mu_{c}) + n_cd\log \sigma_c\right) + \text{const.}
}
and so gradient with respect to $\sigma_m$ is 
\alignStar{
\frac{\partial}{\partial \sigma_m}(- \log p(X\cond y,\Theta)) 
&= \frac{\partial}{\partial \sigma_m}\sum_{c=1}^k \left(\frac{1}{2\sigma_c^2}\sum_{i \in y_c} (x^i - \mu_{c})^T(x^i - \mu_{c}) + n_cd\log \sigma_c\right) + \text{const.}\\
&= \frac{\partial}{\partial \sigma_m} \left(\frac{1}{2\sigma_m^2}\sum_{i \in y_m} (x^i - \mu_{m})^T(x^i - \mu_{m}) + n_md\log \sigma_m\right) + \text{const.}\\
&=  -\frac{1}{\sigma_m^3}\sum_{i \in y_m} (x^i - \mu_{m})^T(x^i - \mu_{m}) + \frac{n_md}{\sigma_m}\\
}
Setting to $0$ and solving for $\sigma_m$ gives:
$$\sigma_m^2 = \frac{1}{n_md}\sum_{i \in y_m} (x^i - \mu_{m})^T(x^i - \mu_{m})$$
If we let $\widehat{X_m}$ be the matrix with rows $x^i - \mu_m$ for all $i \in y_m$, this is equivalent to 
$$\sigma_m^2 = \frac{1}{n_md}\text{Tr}((\widehat{X_m})(\widehat{X_m})^T)$$
}
\item When you run \emph{example\_generative} it loads a variant of the dataset in the figure that has 12 features and 10 classes. This data has been split up into a training and test set, and the code fits a $k$-nearest neighbour classifier to the training set then reports the accuracy on the test data (around $\sim 63\%$ test error). The $k$-nearest neighbour model does poorly here since it doesn't take into account the Gaussian-like structure in feature space for each class label. Write a function \emph{gda} that fits a GDA model to this dataset (using individual full covariance matrices). \blu{Hand in the function and report the test set accuracy}. 
\ans{The training error obtained is 0.332; the test error obtained is 0.369. The code is attached and also provided in Listing \ref{listing: gda}. 
}
\item In this question we would like to replace the Gaussian distribution of the previous problem with the more robust multivariate-t distribution so that it isn't influenced as much by the noisy data.
Unlike the previous case, we don't have a closed-form solution for the parameters. However, if you run \emph{example\_student} it generates random noisy data and fits a multivariate-t model. By using the \emph{studentT} model, write a new function \emph{tda} that implements a generative model that is based on the multivariate-t distribution instead of the Gaussian distribution. \blu{Report the test accuracy  with this model.}
\ans{The training error obtained is 0.177; the test error obtained is 0.195. The code is attached and also provided in Listing \ref{listing: tda}}
}
Hints: you may be able to substantially simplify the notation in the MLE derivations if you use the notation $\sum_{i \in y_c}$ to mean the sum over all values $i$ where $y^i = c$. Similarly, you can use $n_c$ to denote the number of cases where $y_i = c$, so that we have $\sum_{i \in y_c}1 = n_c$. Note that the determinant of a diagonal matrix is the product of the diagonal entries, and the inverse of a diagonal matrix is a diagonal matrix with the reciprocals of the original matrix along the diagonal. 

For the implementation you can use the result from class regarding the MLE of a general multivariate Gaussian. At test time for GDA, you may find it more numerically reasonable to compare log probabilities rather than probabilities of different classes, and you may find it helpful to use the \emph{logdet}  function to compute the log-determinant in a more numerically-stable way than taking the log of the determinant. (Also, don't forget to center at training and test time.)

For the last question, you may find it helpful to define an empty array that can be filled with $k$ \emph{DensityModel} objects  using:
\begin{verbatim}
   subModel = Array{DensityModel}(undef,k)
\end{verbatim}

\begin{lstlisting}[frame=single, caption={GDA},label={listing: gda},language=Julia]%

using JLD, Printf, Statistics, LinearAlgebra
function logGaussianDensity(X)
    (n,d) = size(X)

    # MLE
    mu = (1/n)sum(X,dims=1)'
    Xc = X - repeat(mu',n)
    Sigma = (1/n)*(Xc'Xc)
    SigmaInv = Sigma^-1

    function PDF(Xhat)
        (t,d) = size(Xhat)
        PDFs = zeros(t)

        logZ = (d/2)log(2pi) + (1/2)logdet(Sigma)  
        for i in 1:t
            xc = Xhat[i,:] - mu
            loglik = -(1/2)dot(xc,SigmaInv*xc) - logZ
            PDFs[i] = loglik # compute the log likelihood instead
        end
        return PDFs
    end
    return DensityModel(PDF);
end


function gda_predict(Xhat, prior, PDFs, numClass)
    (n,d) = size(X)
    (t,d) = size(Xhat)

    logPdfs = zeros(size(Xhat, 1), numClass)
    for k = 1:numClass
        logPdfs[:, k] = PDFs[k].pdf(Xhat)
    end

    for i = 1:t
        logPdfs[i, :] += prior
    end

    max_val, max_ind = findmax(logPdfs, dims=2)

    yhat = convert(Array{Int64}, zeros(t))
    for i = 1:t
        yhat[i] = max_ind[i][2] 
    end

    return yhat;
end

function gda(X,y)
    # log prior
    prior = zeros(maximum(y))
    for i in 1:maximum(y)
        prior[i] = log(sum(y .== i)/length(y))
    end
    
    # MLE 
    PDFs = []
    for k = 1:maximum(y)
        Xk = X[y.==k,:]
        yk = convert(Array{Int64, 1}, k*ones(size(Xk, 1))); 
        push!(PDFs, logGaussianDensity(Xk))
    end
    
    # prediction
    predict(Xhat) = gda_predict(Xhat, prior, PDFs, maximum(y))
    return GenericModel(predict);
end


# Load X and y variable
data = load("gaussNoise.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"]);

model = gda(X, y); 

# Evaluate training error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@printf("Train Error with GDA: %.3f\n",trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with GDA: %.3f\n",testError)

Outputs: 
Train Error with GDA: 0.332
Test Error with GDA: 0.369
\end{lstlisting}

\begin{lstlisting}[frame=single, caption={TDA},label={listing: tda},language=Julia]%

using JLD, Printf, Statistics, LinearAlgebra
include("studentT.jl")

function tda_predict(Xhat, prior, subModel, numClass)
    (n,d) = size(X)
    (t,d) = size(Xhat)

    # log likelihood
    logPdfs = zeros(size(Xhat, 1), numClass) 
    for k = 1:numClass
        logPdfs[:, k] = log.(subModel[k].pdf(Xhat))
    end

    # log posteriror
    for i = 1:t
        logPdfs[i, :] += prior
    end

    max_val, max_ind = findmax(logPdfs, dims=2)

    yhat = convert(Array{Int64}, zeros(t))
    for i = 1:t
        yhat[i] = max_ind[i][2] 
    end

    return yhat
end

function tda(X,y)
    # compute prior
    prior = zeros(maximum(y))
    for i in 1:maximum(y)
        prior[i] = log(sum(y .== i)/length(y))
    end
    
    # MLE
    subModel = Array{DensityModel}(undef,maximum(y))
    for k = 1:maximum(y)
        Xk = X[y.==k,:]
        subModel[k] = studentT(Xk) 
    end
    
    # prediction
    predict(Xhat) = tda_predict(Xhat, prior, subModel, maximum(y))
    return GenericModel(predict)
end

# Load X and y variable
data = load("gaussNoise.jld")
(X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

model = tda(X, y); 

# Evaluate training error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@printf("Train Error with student: %.3f\n",trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with student: %.3f\n",testError)


Outputs: 
Train Error with student: 0.177
Test Error with student: 0.195
\end{lstlisting}

%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "root"
%% End: 
