\section{Project Proposal}


For the final part of this assignment, you must a \blu{submit a project proposal} for your course project. The proposal should be a maximum of 2 pages (and 1 page or half of a page is ok if you can describe your plan concisely). The proposal should be written for me and the TAs, so you don't need to introduce any ML background but you will need to introduce non-ML topics. The projects must be done in groups of 2-3. If you are doing your assignment in a group that is different from your project group, only  1 group member should include the proposal as part of their submission (we'll do the merge across assignments, and this means that assignments could have multiple proposals). Please state clearly who is involved with each project proposal.

There is quite a bit of flexibility in terms of the type of project you do, as I believe there are many ways that people can make valuable contributions to research. However, note that ultimately the project will have three parts:
\enum{
\item A very short paper review summarizing the pros and cons of a particular paper on the topic (due with Assignment 4).
\item A short literature review summarizing at least 10 papers on a particular topic (due with Assignment 5).
\item A final report containing at most 6 pages of text (the actual document can be longer due to figures, tables, references, and proofs) that emphasizes a particular ``contribution" (i.e., what doing the project has added to the world).
}
The reason for this, even though it's strange for some possible projects, is that this is the standard way that results are communicated to the research community.

\blu{The three mains ingredients of the project proposal are:
\begin{enumerate}
\item What problem you are focusing on.
\item What you plan to do.
\item What will be the ``contribution".
\end{enumerate}
}
Also, note that for the course project that negative results (i.e., we tried something that we thought we would work in a particular setting but it didn't work) are acceptable (and often unavoidable).

Here are some standard project ``templates" that you might want to follow:

\items{
\item \textbf{Application bake-off}: you pick a specific application (from your research, personal interests, or maybe from Kaggle) or a small number of related applications, and try out a bunch of techniques (e.g., random forests vs. logistic regression vs. generative models). In this case, the contribution would be showing that some methods work better than others for this specific application (or your contribution could be that everything works equally well/badly).
\item \textbf{New application}: you pick an application where ML methods where people aren't using ML, and you test out whether ML methods are effective for the task. In this case, the contribution would be knowing whether ML is suitable for the task.
\item \textbf{Scaling up}: you pick a specific machine learning technique, and you try to figure out how to make it run faster or on larger datasets (for example, how do we apply kernel methods when $n$ is very large). In this case, the contribution would be the new technique and an evaluation of its performance, or could be a comparison of different ways to address the problem.
\item \textbf{Improving performance}: you pick a specific machine learning technique, and try to extend it in some way to improve its performance (for example, how can we efficiently use non-linearity within graphical models). In this case, the contribution would be the new technique and an evaluation of its performance.
\item \textbf{Generalization to new setting}: you pick a specific machine learning technique, and try to extend it to a new setting (for example, making a graphical-model version of random forests).  In this case, the contribution would be the new technique and an evaluation of its performance, or could be a comparison of different ways to address the problem.
\item \textbf{Perspective paper}: you pick a specific topic in ML, read a larger number of papers on the topic, then write a report summarizing what has been done on the topic and what are the most promising directions of future work. In this case, the contribution would be your summary of the relationships between the existing works, and your insights about where the field is going.
\item \textbf{Coding project}: you pick a specific method or set of methods (like independent component analysis), and build an implementation of them. In this case, the contribution could be the implementation itself or a comparison of different ways to solve the problem.
\item \textbf{Theory}: you pick a theoretical topic (like the variance of cross-validation or the convergence of proximal stochastic gradient in the non-convex setting), read what has been done about it, and try to prove a new result (usually by relaxing existing assumptions or adding new assumptions). The contribution could be a new analysis of an existing method, or why some approaches to analyzing the method will not work.
}
The above are just suggestions, and many projects will mix several of these templates together, but if you are having trouble getting going then it's best to stick with one of the above templates. Also note that the above includes topics not covered in the course (like random forests), so there is flexibility in the topic, but the topic should be closely-related to ML.

\blu{This question is mandatory but will not be formally marked: it's just a sanity check that you have at least one project idea that fits within the scope of 540 course project, and it's an excuse for you to allocate some time to thinking about the project.} Also, there is flexibility in the choice of project topics even after the proposal: if you want to explore different topics you can ultimately choose to do a project that is unrelated to the one in your proposal/paper-review/literature-review, although it will likely be easier to do all 4 parts on the same topic.

\newpage
{\color{gre}
  Deep neural networks have achieved an unprecedented success in many fields during the past few years. In particular, the use of deep learning for geometry processing applications is an area of ongoing research. Common problems in this field include segmentation or recognition of 3-D shapes, where the input shape can be represented in many forms, such as triangular meshes, voxels, and point clouds.

  Early attempts in using neural networks for geometry processing included rendering various images of the same geometry from different angles~\cite{su2015multi}, and then using these images as the input to a convolutional neural network. The loss of information caused by this method is intolerable for many applications. Another simple strategy is to represent the 3-D geometry as an implicit function in a 3-D voxel grid~\cite{maturana2015voxnet}. Once again, CNNs can naturally work with such grid based data. Although this voxelization can result in the loss of important surface features, it has been successful in applications such as 3-D tomography.

  Our goal in this project is to review this classical, as well as new network architectures~\cite{qi2017pointnet,atzmon2018point}, for the two tasks of classification and segmentation of 3-D geometry. Although the number of layers and their connection pattern are important properties of neural networks, we would like to mostly focus on the data representation techniques (e.g., triangular surface mesh, point cloud, 2-D rendered images, voxels), and the parameterized inter-layer operators (i.e., generalized convolutions) used in these methods.

  Our contribution would be a combination of \emph{application bake-off} and \emph{perspective paper}. We are planning to first review the important and recent techniques applied to our problem of interest. One of our motivations is that the existing literature is relatively small and tractable in this field (compared to, for example, image processing). We will then select one or two 3-D shape datasets (e.g.,~\cite{chen2009benchmark,hackel2017semantic3d}), and use it to compare the performance of these methods. We are aiming to use the provided code by the authors as much as possible, while being careful not to get into a garbage in garbage out situation.


  % \vspace{2cm}
  % References
  
  % \begin{itemize}
  % \item[[1]] Hang Su, Subhransu Maji, Evangelos Kalogerakis, Erik Learned-Miller, ``Multi-view Convolutional Neural Networks for 3D Shape Recognition'', Proceedings of ICCV 2015
  % \item[[2]] Maturana, Daniel, and Sebastian Scherer. ``Voxnet: A 3d convolutional neural network for real-time object recognition.'' 2015 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS). IEEE, 2015.
  % \item[[3]] Qi, Charles R., et al. ``Pointnet: Deep learning on point sets for 3d classification and segmentation.'' Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2017.
  % \item[[4]] Atzmon, Matan, Haggai Maron, and Yaron Lipman. ``Point convolutional neural networks by extension operators.'' ACM Transactions on Graphics (TOG) 37.4 (2018): 71.
  % \item[[5]] Xiaobai Chen, Aleksey Golovinskiy, and Thomas Funkhouser. ``A Benchmark for 3D Mesh Segmentation'' . ACM Transactions on Graphics (TOG) 28(3), August 2009.
  %   \item[[6]] Hackel, T., et al. ``SEMANTIC3D NET: A New Large-Scale Point Cloud Classificatin Benchmark'' ISPRS Annals of the Photogrammetry, Remote Sensing and Spatial Information Sciences 4 (2017)
  %   \end{itemize}
}

%% Emacs latex configurations; please keep at the end of the file.
%% Local Variables:
%% TeX-master: "root"
%% End: 
