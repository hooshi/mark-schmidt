module a1
using Parameters
using Printf
using LinearAlgebra
include("misc.jl")
include("leastSquares.jl")
include("leastSquaresRBF.jl")
include("findMin.jl")
include("logReg.jl")
include("softMax.jl")
include("plot2Dclassifier.jl")
include("leastAbsolutes.jl")
include("leastMax.jl")
end
