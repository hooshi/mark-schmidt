# Struct to save the RBF basis contruction result
@with_kw struct RBFBasis 
    X::MatFloat
    sigma::Float
    Z::MatFloat
    n::Int
    dx::Int
    dz::Int
end

# evaluate the rbf
function evalRBF(
    rSquared::VecFloat, # Vector of r
    sigma::Float # value of sigma
)::VecFloat # value of the function
    ans = exp. (- rSquared ./ (2*sigma*sigma))
    return ans
end

# Generate an RBF Z matrix
function generateRBFBasis(
    X::MatFloat, # The matrix of data
    X0::MatFloat, # The location of RBF centers
    sigma::Float  # value of sigma
)::RBFBasis # The contructed RBF basis

    n = size(X,1)
    dx = size(X,2)
    dz = size(X0,1)
    @assert( size(X0,2) == size(X,2) )
    Z = zeros(Float, n, dz)

    for i = 1:n
        XimX0 = repeat(X[i,:], dz, 1) - X0 
        ri = sum(XimX0.^2,dims=2)
        zi = evalRBF(ri[:],  sigma)
        Z[i,:] = zi
    end
    
    return RBFBasis(X=X, sigma=sigma,Z=Z,n=n,dx=dx,dz=dz)
end

# Fit data using RBF Basis functions
function leastSquaresRBFL2(
    X::MatFloat, # Training data
    y::VecFloat, # The regression variable
    sigma::Float, # The sigma parameter of basis
    lambda::Float # The regularization parameter
)::LinearModel # The linear model fitted to dat
    
    bs = generateRBFBasis(X, X, sigma)

	# Find regression weights minimizing squared error
    if lambda > 0
	    w = (bs.Z'*bs.Z + lambda*Eye(bs.dz) )\(bs.Z'*y)
    else 
	    w = (bs.Z'*bs.Z)\(bs.Z'*y)
    end

	# Make linear prediction function
	function predict(Xtilde::MatFloat)
        # The RBF centers are the training data
        bs_eval = generateRBFBasis(Xtilde, X, sigma)
        return bs_eval.Z * w
    end
    
	# Return model
	return LinearModel(predict,w)
end
