function softMaxObj(
    ;X::MatFloat, # Training data n x d
    W::MatFloat,  # The weight matrix n x c
    Y::MatFloat)  # The observed probabilities (n x c)
    n = size(X,1)
    d = size(X,2)
    c = size(Y,2)

    # dimensions must match
    @assert( size(X,1) == size(Y,1) )
    # sum of rows of Y should be one
    @assert( norm( sum(Y,dims=2) - ones(n) ) < 1e-10 )

    vec(X_) = X_[:]

    # Scale data to prevent numerical issues, this will not affect the final
    # value of the objective
    S = X*W
    s = maximum(S,dims=2)
    S = S - repeat(s,1,c)

    # Evaluate objective and gradient
    expS = exp.(S)
    sumExpS = sum(expS,dims=2)

    obj = -vec(Y)'*vec(S) + sum(log.(sumExpS)) 
    grad = X' * (-Y + expS ./ repeat(sumExpS,1,c))

    obj = obj/n
    grad = grad/n

    return (obj,grad)
end

function softMaxClassifier(
    X::MatFloat, # trainng data
    y::VecInt; # labels
    verbose=false)
    n = size(X,1)
    d = size(X,2)
    @assert(size(y,1) == n)
    y = y .- minimum(y[:]) .+ 1
    c = convert(Int, maximum(y))

    Y = zeros(n,c)
    for citer in 1:c
        Y[y .== citer, citer] .= 1.0
    end
    
	function _objAndGrad(w_)
        W_ = reshape(w_,d,c)
        (f,G) = softMaxObj(X=X,W=W_,Y=Y)
        return (f,reshape(G,:,1))
    end

    (u,v) = _objAndGrad(zeros(d*c))
    
	W = zeros(d,c)
	W[:] = findMin(_objAndGrad,W[:],verbose=verbose, derivativeCheck=true)

	predict(Xhat) = mapslices(argmax,Xhat*W,dims=2)
    

	return LinearModel(predict,W)   
end
