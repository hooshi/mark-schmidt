import JLD
using Statistics
using Random
using Printf
include("../a1.jl")

function main()
    n =100
    c = 20
    d = 40    
    Y = randn(n,c) 
    X = randn(n,d)
    W = randn(d,c)
    delta = randn(d,c)
    Y = Y ./ repeat(sum(Y,dims=2),1,c)
    
    obj = nothing
    grad =  nothing
    function objEval(win)
        W = reshape(win,d,c)
        (obj,grad) = a1.softMaxObj(X=X,W=W,Y=Y)
        # @printf("%s %s \n", obj, size(grad))
        return obj
    end
    function gradEval(delta)
        # @printf("%s %s \n", obj, size(grad))
        return delta[:]' * grad[:] 
    end
    a1.derivTest(W[:], delta[:], objEval, gradEval; numHalving=20)
end


# deal with no clear()
main()
