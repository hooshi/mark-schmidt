import JLD
import Statistics
include("a1.jl")

function main()
    # Load X and y variable
    data = JLD.load("multiData.jld")
    (X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

    # Data is already roughly standardized, but let's add bias
    n = size(X,1)
    X = [ones(n,1) X]
    X = convert(a1.MatFloat, X)
    y = convert(a1.VecInt, y)

    # Do the same transformation to the test data
    t = size(Xtest,1)
    Xtest = [ones(t,1) Xtest]

    # Fit one-vs-all logistic regression model
    # model = a1.logRegOnevsAll(X,y)
    model = a1.softMaxClassifier(X,y;verbose=true)

    # Compute training and validation error
    yhat = model.predict(X)
    trainError = Statistics.mean(yhat .!= y)
    @show(trainError)
    yhat = model.predict(Xtest)
    validError = Statistics.mean(yhat .!= ytest)
    @show(validError)

    # Plot results
    k = maximum(y)
    a1.plot2Dclassifier(X,y,model,Xtest=Xtest,ytest=ytest,biasIncluded=true,k=5)

    PyPlot.savefig("_saved_fig.pdf",bbox_inches="tight")
end


# deal with no clear()
main()
