import JLD
import Printf
import PyPlot
import Random
using Parameters
include("a1.jl")

# Train a model
function train(X,y, sigma, lambda)
    return a1.leastSquaresRBFL2(X,y[:],sigma, lambda)
end

# Get the error an a set of data
function getError(regressor, X, y; shouldPlot=false)
    t = size(X,1)
    yhat = regressor.predict(X)
    error = sum((yhat - y).^2)/t

    if shouldPlot
        PyPlot.figure(0)
        PyPlot.clf()
        PyPlot.plot(X,y,"g.")
        Xhat = minimum(X):.1:maximum(X)
        Xhat = convert(a1.MatFloat, reshape(Xhat,length(Xhat),1))
        yhat = regressor.predict(Xhat)
        PyPlot.plot(Xhat[:],yhat,"r")
        PyPlot.ylim((-300,400))
    end
    return error
end

# Shuffled range from 1 to len
function shuffledRange(len)
    fixed_seed = 65465454;
    return Random.randperm( Random.MersenneTwister(fixed_seed), len )
end

# Store results of training and validation
@with_kw struct Result
    sigma
    lambda
    trainError
    validError
end

function main()
    data = JLD.load("nonLinear.jld")
    (X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

    #Shuffle and create training and validation set
    nSamples = size(X,1)
    randomIds = shuffledRange(nSamples)
    Xtrain = X[randomIds[1:Int(nSamples/2)],:]
    Xvalid = X[randomIds[Int(nSamples/2):end],:]
    ytrain = y[randomIds[1:Int(nSamples/2)],:]
    yvalid = y[randomIds[Int(nSamples/2):end],:]

    # Values of the hyper-params to be tested
    # Values out of this range are really horrible
    labmdaVals = [1e-5,1e-4,1e-3,1e-2,1e-1,1e0]
    sigmaVals =  [0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    results = []

    # Train and eval validation error for each hyper param combination
    for lambda in labmdaVals
        for sigma in sigmaVals
            model = train(Xtrain, ytrain, sigma, lambda)
            trainError = getError(model, Xtrain, ytrain)
            validError = getError(model, Xvalid, yvalid, shouldPlot=false)
            result = Result(sigma=sigma, lambda=lambda, trainError=trainError, validError=validError)
            push!(results, result)
        end
    end

    # sort the resutls so that the lowest validation error goes last
    sort_lt(x,y) = isless(y.validError,x.validError) # bigger first
    sort!(results, lt=sort_lt)

    # Print the results
    Printf.@printf("%10s %10s %10s %10s \n","lambda", "sigma", "train", "valid")
    for r in results
        Printf.@printf("%10.3e %10.10g %10.10g %10.10g \n",
                       r.lambda, r.sigma, r.trainError, r.validError)
    end


    # Final training on all data
    finalModel = train(X, y, results[end].sigma, results[end].lambda)
    trainError = getError(finalModel, X, y)
    testError = getError(finalModel, Xtest, ytest, shouldPlot=true)
    Printf.@printf( "Final model: \n")
    Printf.@printf( "Sigma=%g, Lambda=%g\n",results[end].sigma, results[end].lambda)
    Printf.@printf( "trainError(all data): %g testError: %g (only test data shown)\n",
                    trainError, testError)

    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.title("leastSquaresRBF2 Sigma=0.6, Lambda=1e-5\n")
    PyPlot.savefig("_saved_fig.pdf",bbox_inches="tight")

end

main()
