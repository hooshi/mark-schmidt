# Load X and y variable
import JLD
import Printf
import PyPlot

function main()
    include("a1.jl")

    data = JLD.load("nonLinear.jld")
    (X,y,Xtest,ytest) = (data["X"],data["y"],data["Xtest"],data["ytest"])

    # Compute number of training examples and number of features
    (n,d) = size(X)

    # Fit least squares model

    #model = a1.leastSquares(X,y)
    #model = a1.leastSquaresRBFL2(X,y[:],0.5,0.0001)
    model = a1.leastSquaresRBFL2(X,y[:],1.0,1.0)

    # Report the error on the test set
    t = size(Xtest,1)
    yhat = model.predict(Xtest)
    testError = sum((yhat - ytest).^2)/t
    Printf.@printf("TestError = %.2f\n",testError)

    # Plot model
    PyPlot.figure()
    PyPlot.plot(X,y,"b.")
    PyPlot.plot(Xtest,ytest,"g.")
    Xhat = minimum(X):.1:maximum(X)
    Xhat = convert(a1.MatFloat, reshape(Xhat,length(Xhat),1)) # Make into an n by 1 matrix
    #@printf("Xhat : %s \n", Xhat);
    #@printf("w : %s \n", model.w);
    yhat = model.predict(Xhat)
    PyPlot.plot(Xhat[:],yhat,"r",linewidth=2)
    PyPlot.ylim((-300,400))

    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.title("leastSquaresRBFL2 results with lambda=sigma=1")
    PyPlot.savefig("_saved_fig.pdf",bbox_inches="tight")

end


main()
