import MathProgBase
import Clp

function leastMax(X,y)

    # Setup the linear Program
	n = size(X,1)
	Z = [ones(n,1) X]
	d = size(Z,2)

    c  = zeros(1+d,1)
    c[1] = 1 

    A  = zeros(2*n,1+d)
    #
    A[1:n,1] = ones(n,1)
    A[(n+1):(2*n),1] = ones(n,1)
    #
    A[1:n,(2):(d+1)] = Z
    A[(n+1):(2*n),(2):(d+1)] = -Z

    v  = zeros(2*n,1)
    v[1:n] = y 
    v[(n+1):(2*n)] = -y 

    # Solve the linprog
    solution = MathProgBase.linprog(c[:], A, v[:], Inf, -Inf, Inf, Clp.ClpSolver())
    @printf("Lingprog solved\n")
    @printf("objective = %.10g \n", solution.objval)
    @printf("status = %s \n", solution.status)
    
    w = solution.sol[(1+1):(1+d)]

	# Make linear prediction function
	predict(Xtilde) = [ones(size(Xtilde,1),1) Xtilde]*w

	# Return model
	return LinearModel(predict,w)
end

